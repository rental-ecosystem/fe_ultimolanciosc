import { socialRoutes } from "./routes";
import { ListInlineItem } from 'reactstrap';

export const getSocialRoutesIcons = () => {
  return socialRoutes.map(({ path, name, icon }) => {
    return (
      <ListInlineItem key={name}>
        <i
          className={`${icon} fa-lg`}
          onClick={() => window.open(path)}
        />
      </ListInlineItem>
    )
  })
};
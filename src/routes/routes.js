// import React from 'react';
import { Home } from "../components/Home";
import { AboutUs } from "../components/AboutUs";
import { Contacts } from "../components/Contacts";

export function CustomComponent(Component) {
  return function (props) {
    return (
      <Component {...props}/>
    );
  }
}

export const routes = [
  {
    path: "/home",
    name: "Home",
    component: Home,
    showInSidebar: true
  },
  {
    path: "/about-us",
    name: "About us",
    component: AboutUs,
    showInSidebar: true
  },
  {
    path: "/contacts",
    name: "Contacts",
    component: Contacts,
    showInSidebar: true
  }
];

export const socialRoutes = [
  {
    path: "https://www.facebook.com/Ultimolanciospinningclub/",
    name: "Facebook",
    icon: "fab fa-facebook-f"
  },
  {
    path: "https://www.instagram.com/ultimolanciospinningclub/",
    name: "Instagram",
    icon: "fab fa-instagram"
  },
  {
    path: "https://www.youtube.com/channel/UCnyvEU3hKx-ehGS_O7AtC3A/featured",
    name: "Youtube",
    icon: "fab fa-youtube"
  }
];
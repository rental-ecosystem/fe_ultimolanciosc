import React from 'react';
// import PropTypes from 'prop-types';
import { Header } from "./Header";
import { Main } from "./Main";
import { Container } from "reactstrap";
// import Footer from "./Footer";

export const Layout = props => {
  return (
    <div className="layout">
      <Header/>
      <Container>
        <Main/>
        {/*<Footer/>*/}
      </Container>
    </div>
  );
};

Layout.propTypes = {};

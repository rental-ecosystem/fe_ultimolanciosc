import React from 'react';
// import PropTypes from 'prop-types';
import logo from '../assets/logo/square.jpeg'
import '../css/header.scss'
import { /*getRoutes,*/ routes } from "../routes/routes";
import { useHistory } from "react-router";
import { Col, Container, Row } from 'reactstrap';
import { List, ListInlineItem } from 'reactstrap';
import { getSocialRoutesIcons } from "../routes/utils";


export const Header = props => {

  const history = useHistory();

  const routeTo = (route) => {
    localStorage.setItem('selected-page', route.path);
    history.push(route.path)
  };

  const navigationBar = () => {
    return routes.filter(r => r.showInSidebar).map(r => (
      <ListInlineItem onClick={() => routeTo(r)} key={`sidebar_${r.name}`}>
        {r.name.toUpperCase()}
      </ListInlineItem>
    ))
  };

  return (
    <div id="header">
      <Row>
        <Col id="header__navigation">
          <Container>
            <Row>
              <Col className="internal-routes"
                lg={6}
              >
                <List>
                  {navigationBar()}
                </List>
              </Col>
              <Col className="social-routes"
                lg={6}
              >
                <List>
                  {getSocialRoutesIcons()}
                </List>
              </Col>
            </Row>
          </Container>
        </Col>
      </Row>
      <Row>
        <Col id="header__image">
          <img src={logo} alt="logo"/>
        </Col>
      </Row>
    </div>
  );
};

Header.propTypes = {};

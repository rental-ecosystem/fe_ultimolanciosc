import React from 'react';
// import PropTypes from 'prop-types';
import { Redirect, Route, Switch } from "react-router";
import { routes } from "../routes/routes";

export const Main = () => {

  const getRouteComponent = () => {
    return routes.map(({ path, name, component }) => <Route key={name} path={path} component={component} />)
  };

  const page = localStorage.getItem('selected-page');

  return (
    <div id="main">
      <Switch>
        {getRouteComponent()}
        <Redirect to={page ? page : '/home'} />
      </Switch>
    </div>
  );
};

Main.propTypes = {};

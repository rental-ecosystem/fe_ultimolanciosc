import React from 'react';
// import PropTypes from 'prop-types';

const YoutubeIFrame = () => {
  return (
    <div>
      <iframe
        title="ytplayer"
        id="ytplayer"
        // type="text/html"
        width="640"
        height="360"
        src="https://www.youtube.com/embed/WpPS5QEjmvM&t=4s"
        frameBorder="0"
      />
    </div>
  );
};

YoutubeIFrame.propTypes = {};

export default YoutubeIFrame;
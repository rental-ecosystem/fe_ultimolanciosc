import React from 'react';
import { Col, Row } from 'reactstrap';
// import PropTypes from 'prop-types';
// import PhotoCarousel from './PhotoCarousel';
import YoutubeIFrame from "./YoutubeIFrame";


export const Home = () => {

  return (
    <div id="home">
      <Row>
        <Col>
          {/*<PhotoCarousel />*/}
          <YoutubeIFrame/>
        </Col>
      </Row>
    </div>
  );
};

Home.propTypes = {};
